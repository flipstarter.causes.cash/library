import type { ClientState } from './enums';
import type ElectrumClient from './electrum-client';
import type { RPCError } from './rpc-interfaces';

export interface ClientConfig
{
	// Availability of the client's connection
	state: ClientState;

	// The client's connection
	connection: ElectrumClient;
}

/**
 * A list of possible responses to requests.
 */
export type RequestResponse = object | string | number | boolean | null | RequestResponse[];

// Request resolvers are used to process the response of a request. This takes either
// an error object or any stringified data, while the other parameter is omitted.
export type RequestResolver = (error?: Error, data?: string) => void;

// Promise types
export type ResolveFunction<T> = (value: T | PromiseLike<T>) => void;
export type RejectFunction = (reason?: any) => void;

export interface VersionRejected
{
	error: RPCError;
}

export interface VersionNegotiated
{
	software: string;
	protocol: string;
}

export const isVersionRejected = function(object: any): object is VersionRejected
{
	return 'error' in object;
};

export const isVersionNegotiated = function(object: any): object is VersionNegotiated
{
	return 'software' in object && 'protocol' in object;
};

/**
 * Possible Transport Schemes for communication with the Electrum server
 */
export type TransportScheme = 'tcp' | 'tcp_tls' | 'ws' | 'wss';

// Connection options used with TLS connections.
export interface ConnectionOptions
{
	rejectUnauthorized?: boolean;
	serverName?: string;
}
