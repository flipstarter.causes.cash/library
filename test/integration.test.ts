// Load the testing framework.
import { expect, test } from 'vitest';
import { UseCase, useCases } from './use-cases';
import { ElectrumClient, ElectrumCluster, ElectrumTransport } from '../lib';

// Declare use case as a global-scope reference variable.
let useCase: UseCase;

// Set up client request test
const testClientRequest = async function(): Promise<void>
{
	// Initialize an electrum client.
	const electrum = new ElectrumClient('Electrum client test', '1.4.1', 'bch.imaginary.cash');

	// Wait for the client to connect
	await electrum.connect();

	// Perform the request according to the use case.
	// @ts-ignore
	const requestOutput = await electrum.request(...useCase.request.input);

	// Close the connection synchronously.
	await electrum.disconnect();

	// Verify that the transaction hex matches expectations.
	expect(requestOutput).toEqual(useCase.request.output);
};

// Set up cluster request test
const testClusterRequest = async function(): Promise<void>
{
	// Initialize an electrum cluster where 2 out of 3 needs to be consistent, polled randomly with fail-over (default).
	const electrum = new ElectrumCluster('Electrum cluster example', '1.4.1', 2, 3);

	// Add some servers to the cluster.
	electrum.addServer('bch.imaginary.cash');
	electrum.addServer('electroncash.de');
	electrum.addServer('electroncash.dk');
	electrum.addServer('electron.jochen-hoenicke.de', 51002);
	electrum.addServer('electrum.imaginary.cash');

	// Wait for enough connections to be available.
	await electrum.ready();

	// Perform the request according to the use case.
	// @ts-ignore
	const requestOutput = await electrum.request(...useCase.request.input);

	// Close all connections synchronously.
	await electrum.shutdown();

	// Verify that the transaction hex matches expectations.
	expect(requestOutput).toEqual(useCase.request.output);
};

// Set up unencrypted client request test
const testUnencryptedClientRequest = async function(): Promise<void>
{
	// Initialize an electrum client.
	const electrum = new ElectrumClient('Electrum client test', '1.4.1', 'bch.imaginary.cash', ElectrumTransport.TCP.Port, ElectrumTransport.TCP.Scheme);

	// Wait for the client to connect
	await electrum.connect();

	// Perform the request according to the use case.
	// @ts-ignore
	const requestOutput = await electrum.request(...useCase.request.input);

	// Close the connection synchronously.
	await electrum.disconnect();

	// Verify that the transaction hex matches expectations.
	expect(requestOutput).toEqual(useCase.request.output);
};

// Set up client request test using WebSockets
const testWebSocketClientRequest = async function(): Promise<void>
{
	// Initialize an electrum client.
	const electrum = new ElectrumClient('Electrum client test', '1.4.1', 'bch.imaginary.cash', ElectrumTransport.WSS.Port, ElectrumTransport.WSS.Scheme);

	// Wait for the client to connect
	await electrum.connect();

	// Perform the request according to the use case.
	// @ts-ignore
	const requestOutput = await electrum.request(...useCase.request.input);

	// Close the connection synchronously.
	await electrum.disconnect();

	// Verify that the transaction hex matches expectations.
	expect(requestOutput).toEqual(useCase.request.output);
};

// Set up cluster request test using WebSockets
const testWebSocketClusterRequest = async function(): Promise<void>
{
	// Initialize an electrum cluster where 2 out of 3 needs to be consistent, polled randomly with fail-over (default).
	const electrum = new ElectrumCluster('Electrum cluster example', '1.4.1', 2, 3);

	// Add some servers to the cluster.
	electrum.addServer('bch.imaginary.cash', ElectrumTransport.WSS.Port, ElectrumTransport.WSS.Scheme);
	electrum.addServer('electroncash.de', 60002, ElectrumTransport.WSS.Scheme);
	// electrum.addServer('blackie.c3-soft.com', ElectrumTransport.WSS.Port, ElectrumTransport.WSS.Scheme);
	electrum.addServer('bch.loping.net', ElectrumTransport.WSS.Port, ElectrumTransport.WSS.Scheme);
	electrum.addServer('electrum.imaginary.cash', ElectrumTransport.WSS.Port, ElectrumTransport.WSS.Scheme);

	// Wait for enough connections to be available.
	await electrum.ready();

	// Perform the request according to the use case.
	// @ts-ignore
	const requestOutput = await electrum.request(...useCase.request.input);

	// Close all connections synchronously.
	await electrum.shutdown();

	// Verify that the transaction hex matches expectations.
	expect(requestOutput).toEqual(useCase.request.output);
};

// Set up unencrypted client request test using WebSockets
const testUnencryptedWebSocketClientRequest = async function(): Promise<void>
{
	// Initialize an electrum client.
	const electrum = new ElectrumClient('Electrum client test', '1.4.1', 'bch.imaginary.cash', ElectrumTransport.WS.Port, ElectrumTransport.WS.Scheme);

	// Wait for the client to connect
	await electrum.connect();

	// Perform the request according to the use case.
	// @ts-ignore
	const requestOutput = await electrum.request(...useCase.request.input);

	// Close the connection synchronously.
	await electrum.disconnect();

	// Verify that the transaction hex matches expectations.
	expect(requestOutput).toEqual(useCase.request.output);
};

const testRestartedClient = async function(): Promise<void>
{
	// Initialize an electrum client.
	const electrum = new ElectrumClient('Electrum client test', '1.4.1', 'bch.imaginary.cash');

	// Wait for the client to connect.
	await electrum.connect();

	// Close the connection.
	await electrum.disconnect();

	// Reconnect the client.
	await electrum.connect();

	// Perform the request according to the use case.
	// @ts-ignore
	const requestOutput = await electrum.request(...useCase.request.input);

	// Close the connection synchronously.
	await electrum.disconnect();

	// Verify that the transaction hex matches expectations.
	expect(requestOutput).toEqual(useCase.request.output);
};

const testRestartedWebSocketCluster = async function(): Promise<void>
{
	// Initialize an electrum cluster where 2 out of 3 needs to be consistent, polled randomly with fail-over.
	const electrum = new ElectrumCluster('CashScript Application', '1.4.1', 2, 3);

	// Add some servers to the cluster.
	electrum.addServer('bch.imaginary.cash', 50004, ElectrumTransport.WSS.Scheme);
	electrum.addServer('electroncash.de', 60002, ElectrumTransport.WSS.Scheme);
	// electrum.addServer('blackie.c3-soft.com', 50004, ElectrumTransport.WSS.Scheme);
	electrum.addServer('bch.loping.net', 50004, ElectrumTransport.WSS.Scheme);
	electrum.addServer('electrum.imaginary.cash', 50004, ElectrumTransport.WSS.Scheme);

	// Wait for enough connections to be available.
	await electrum.ready();

	// Close all connections.
	await electrum.shutdown();

	// Restart the cluster.
	await electrum.startup();

	// Wait for enough connections to be available.
	await electrum.ready();

	// Perform the request according to the use case.
	// @ts-ignore
	const requestOutput = await electrum.request(...useCase.request.input);

	// Close all connections synchronously.
	await electrum.shutdown();

	// Verify that the transaction hex matches expectations.
	expect(requestOutput).toEqual(useCase.request.output);
};

const testRequestAbortOnConnectionLossClient = async function(): Promise<void>
{
	// Initialize an electrum client.
	const electrum = new ElectrumClient('Electrum client test', '1.4.1', 'bch.imaginary.cash');

	// Wait for the client to connect.
	await electrum.connect();

	// Perform the request according to the use case (but do not await it).
	// @ts-ignore
	const requestPromise = electrum.request(...useCase.request.input);

	// Immediately close the underlying connection.
	await electrum.connection.disconnect();

	// Check that the request promise resolves with an Error indicating lost connection.
	expect(await requestPromise instanceof Error).toBeTruthy();
	expect((await requestPromise as Error).message).toEqual('Connection lost');
};

const testRequestAbortOnConnectionLossCluster = async function(): Promise<void>
{
	// Initialize an electrum cluster where 2 out of 3 needs to be consistent, polled randomly with fail-over (default).
	const electrum = new ElectrumCluster('Electrum cluster example', '1.4.1', 2, 3);

	// Add some servers to the cluster.
	electrum.addServer('bch.imaginary.cash');
	electrum.addServer('electroncash.de');
	electrum.addServer('electroncash.dk');
	electrum.addServer('electron.jochen-hoenicke.de', 51002);
	electrum.addServer('electrum.imaginary.cash');

	// Wait for enough connections to be available.
	await electrum.ready();

	// Perform the request according to the use case (but do not await it).
	// @ts-ignore
	const requestPromise = electrum.request(...useCase.request.input);

	// Wait for 1 ms so that the request was sent to all clients.
	// eslint-disable-next-line no-promise-executor-return
	await new Promise((resolve) => setTimeout(resolve, 1));

	// Retrieve an array of all clients in the cluster.
	const clients = Object.values(electrum.clients);

	// Close the underlying connection for all clients in the array.
	await Promise.all(clients.map((client) => client.connection.connection.disconnect()));

	// Check that the request promise resolves with an Error indicating lost connection.
	expect(await requestPromise instanceof Error).toBeTruthy();
	expect((await requestPromise as Error).message).toEqual('Connection lost');
};

const testConnectOnAlreadyConnectedClient = async function(): Promise<void>
{
	// Initialize an electrum client.
	const electrum = new ElectrumClient('Electrum client test', '1.4.1', 'bch.imaginary.cash');

	// Invoke connect the first time and wait for it to be connected.
	await electrum.connect();

	// Attempt to invoke it a second time despite being already connected.
	// NOTE: It should return immediately as it is already connected and should not throw any errors in subsequent calls.
	await electrum.connect();

	// Perform a request according to the use case to ensure everything works as expected.
	// @ts-ignore
	const requestOutput = await electrum.request(...useCase.request.input);

	// Close the connection synchronously.
	await electrum.disconnect();

	// Verify that the transaction hex matches expectations.
	expect(requestOutput).toEqual(useCase.request.output);
};

const testSimultaneousConnectsOnClient = async function(): Promise<void>
{
	// Initialize an electrum client.
	const electrum = new ElectrumClient('Electrum client test', '1.4.1', 'bch.imaginary.cash');

	// Invoke multiple connects at once but DO NOT await them as want to test that this call can be made simultaneously.
	electrum.connect();
	electrum.connect();
	electrum.connect();

	// Now finally make one more connect call but this time await it.
	await electrum.connect();

	// Perform a request according to the use case to ensure everything works as expected.
	// @ts-ignore
	const requestOutput = await electrum.request(...useCase.request.input);

	// Close the connection synchronously.
	await electrum.disconnect();

	// Verify that the transaction hex matches expectations.
	expect(requestOutput).toEqual(useCase.request.output);
};

const testSimultaneousDisconnectsOnClient = async function(): Promise<void>
{
	// Initialize an electrum client.
	const electrum = new ElectrumClient('Electrum client test', '1.4.1', 'bch.imaginary.cash');

	// Invoke connect the first time and wait for it to be connected.
	await electrum.connect();

	// Invoke multiple disconnects at once but DO NOT await them as want to test that this call can be made simultaneously.
	electrum.disconnect();
	electrum.disconnect();
	electrum.disconnect();

	// Now finally make one more disconnect call but this time await it.
	await electrum.disconnect();

	// Connect again so that we can test our sample test-case.
	await electrum.connect();

	// Perform a request according to the use case to ensure everything works as expected.
	// @ts-ignore
	const requestOutput = await electrum.request(...useCase.request.input);

	// Close the connection synchronously.
	await electrum.disconnect();

	// Verify that the transaction hex matches expectations.
	expect(requestOutput).toEqual(useCase.request.output);
};

const testSimultaneousDisconnectAndConnectOnClient = async function(): Promise<void>
{
	// Initialize an electrum client.
	const electrum = new ElectrumClient('Electrum client test', '1.4.1', 'bch.imaginary.cash');

	// Connect to our Electrum Client and await it to make sure it is successful.
	await electrum.connect();

	// Invoke a disconnect but do not await it so that we can make it race with the below connect() call.
	electrum.disconnect();

	// Connect to our Electrum Client.
	await electrum.connect();

	// Perform a request according to the use case to ensure everything works as expected.
	// @ts-ignore
	const requestOutput = await electrum.request(...useCase.request.input);

	// Close the connection synchronously.
	await electrum.disconnect();

	// Verify that the transaction hex matches expectations.
	expect(requestOutput).toEqual(useCase.request.output);
};

// Set up normal tests.
const runNormalTests = async function(): Promise<void>
{
	// For each use case to test..
	for(const currentUseCase in useCases)
	{
		// .. assign it to the use case global reference.
		useCase = useCases[currentUseCase];

		test('Request data from client', testClientRequest);
		test('Request data from cluster', testClusterRequest);
		test('Request data from unencrypted client', testUnencryptedClientRequest);
		test('Request data from client using WebSocket connection', testWebSocketClientRequest);
		test('Request data from cluster using WebSocket connection', testWebSocketClusterRequest);
		test('Request data from unencrypted client using WebSocket connection', testUnencryptedWebSocketClientRequest);
		test('Request data after restarting client', testRestartedClient);
		test('Request data after restarting WebSocket cluster', testRestartedWebSocketCluster);
		test('Abort active request on connection loss for a client', testRequestAbortOnConnectionLossClient);
		test('Abort active request on connection loss for a cluster', testRequestAbortOnConnectionLossCluster);
		test('Test connect() on already connected client', testConnectOnAlreadyConnectedClient);
		test('Test simultaneous connect() calls on client', testSimultaneousConnectsOnClient);
		test('Test simultaneous disconnect() calls on client', testSimultaneousDisconnectsOnClient);
		test('Test simultaneous disconnect() and connect() calls on client', testSimultaneousDisconnectAndConnectOnClient);
	}
};

const runTests = async function(): Promise<void>
{
	// Run normal tests.
	await runNormalTests();
};

// Run all tests.
runTests();
